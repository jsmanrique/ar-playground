/* ========================================================================
 * brick.js v0.0.1
 * Draw a brick in an A-Frame element
 * ========================================================================
 * Copyright 2019 Jose Manrique Lopez de la Fuente
 * Licensed under MIT (../LICENSE)
 * ======================================================================== */

var Brick = (function(){
    var draw = function(el, x, y, z, color) {
        var box = document.createElement('a-box');
        box.setAttribute('color', color);
        box.setAttribute('rotation', '0 0 0');
        box.setAttribute('position', x + ' ' + y + ' ' + z);
        el.appendChild(box)
        var cylinder = document.createElement('a-cylinder');
        cylinder.setAttribute('color', color);
        cylinder.setAttribute('rotation', '90 0 0');
        cylinder.setAttribute('radius', '0.33');
        cylinder.setAttribute('height', '0.5');
        cylinder.setAttribute('position', x + ' ' + y + ' ' + (z+0.33));
        el.appendChild(cylinder);
    };

    return {
        draw: draw
    };

})();