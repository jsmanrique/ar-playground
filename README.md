# Some experiments with WebVR and AR

Most of the pages here use [A-Frame](https://aframe.io/) and for those with AR
(Augmented Reality), I am using [ar.js](https://github.com/jeromeetienne/ar.js),
and `kanji` as marker pattern.

Some examples:
* [Bitergian owl](https://jsmanrique.gitlab.io/ar-playground/bitergia.html)
* [AR Bitergia owl](https://jsmanrique.gitlab.io/ar-playground/bitergia-ar.html)
* [Liferay logo](https://jsmanrique.gitlab.io/ar-playground/liferay.html)
* [AR Liferay logo](https://jsmanrique.gitlab.io/ar-playground/liferay-ar.html)